"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import SimpleBar from 'simplebar'; // Кастомный скролл
import tabs from './components/tabs.js'; // Tabs
import Choices from 'choices.js'; // Select plugin
import intlTelInput from 'intl-tel-input';
import {WebpMachine} from "webp-hero"
import AirDatepicker from 'air-datepicker';
import {addTouchClass, fullVHfix} from "./components/functions.js";

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Phone field
const input = document.querySelector('#phone');
if (input) {
    intlTelInput(input, {
        initialCountry: "ru",
        nationalMode: true,
        formatOnDisplay: true,
        separateDialCode: true,
        hiddenInput: "full_number",
        localizedCountries: { 'ru': 'Russian' },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.min.js",
    });
}

// Mask inputs
// maskInput('input[name="phone"]'); // phone
maskInput('[data-card-num]', '____ ____ ____ ____'); // card number
maskInput('[data-card-date]', '__/___'); // card date
//maskInput('[data-date]', '__/__/____'); // card date


// Вкладки (tabs)
tabs();

function isNumberKey(evt){
    let charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}



// Custom Select
document.querySelectorAll('.pretty-select').forEach(el => {
    const prettySelect = new Choices(el,{
        allowHTML: true,
        searchEnabled: false
    });
});

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
	document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
		new SimpleBar(scrollBlock, {
			autoHide: false
		});
	});
}

// Input Date {
document.querySelectorAll('[data-date]').forEach(el => {
    new AirDatepicker(el,{
        autoClose: true
    });
});

document.querySelectorAll('[data-calendar]').forEach(el => {
    new AirDatepicker(el,{
        inline: true
    });
});

document.addEventListener("DOMContentLoaded", function() {

    if(document.querySelector('[data-doc-upload]')){
        document.querySelector('[data-doc-upload]').classList.add('app-doc--uploaded');
    }
});


